-- SELECT 기본 문법 및 연산자

-- 모든 행 모든 컬럼 조회
-- EMPLOYEE 테이블에서 모든 정보 조회
-- 앞으로 모든 예제는 코딩 컨벤션을 지킬 것이다.
SELECT
       *
  FROM EMPLOYEE;
  
CREATE TABLE CHARTEST(
   CH1 CHAR(6),
   CH2 CHAR(9),
   CH3 CHAR(3),
   CH4 CHAR(3)
);
  
INSERT
  INTO CHARTEST
VALUES
(
  '김치'
, '김치'
, '김'
, ''
);

SELECT
       *
  FROM CHARTEST;

-- 커밋하기
COMMIT;

-- 문자가 들어있으면 갯수로, 남는 건 바이트로 인식
SELECT
       LENGTH(CH1)  
     , LENGTH(CH2)   -- 2글자 + 남는 3바이트 -> 5
     , LENGTH(CH3)
     , LENGTH(CH4)
  FROM CHARTEST;

-- 글자가 들어있는 컬럼의 바이트 수
SELECT
       LENGTHB(CH1)  
     , LENGTHB(CH2)  
     , LENGTHB(CH3)
     , LENGTHB(CH4)
  FROM CHARTEST;

-- 삭제 해보기
DELETE
  FROM CHARTEST;
  
-- 삭제 후 조회하기
SELECT
       *
  FROM CHARTEST;

-- 롤백하기
ROLLBACK;

-- 본격적인 SELECT 예제 시작 ----------------------
-- 모든 사원 모든 컬럼 조회
SELECT
       *
  FROM EMPLOYEE;

-- 사원들의 사번(사원번호), 이름 조회
SELECT
       EMP_ID
     , EMP_NAME
  FROM EMPLOYEE;

-- 원하는 행(튜플) 조회
-- 201번 사번의 사번 및 이름 조회 
SELECT
       EMP_ID
     , EMP_NAME
  FROM EMPLOYEE
 WHERE EMP_ID = '201';
 
-- EMPLOYEE 테이블에서 부서 코드가 'D9'인 사원 조회
SELECT
       *
  FROM EMPLOYEE
 WHERE DEPT_CODE = 'D9';
 
-- EMPLOYEE 테이블에서 직급 코드가 'J1'인 사원 조회
SELECT
       *
  FROM EMPLOYEE
 WHERE JOB_CODE = 'J1';
 
-- 원하는 행과 컬럼 조회
-- EMPLOYEE 테이블에서 급여가 300만원 이상(>=)인 사원의
-- 사번, 이름, 부서코드, 급여를 조회하세요
SELECT
       EMP_ID
     , EMP_NAME
     , DEPT_CODE
     , SALARY
  FROM EMPLOYEE
 WHERE SALARY >= 3000000;
 
-- WHERE절
-- 테이블에서 조건을 만족하는 값을 가진 행(튜플)을 골라냄
-- 여러 개의 조건을 만족하는 행을 골라 낼 때는 AND 혹은 OR을 사용할 수 있다.
 
-- 부서코드가 D6이고 급여를 300만원보다 많이 받는 직원의
-- 이름, 부서코드, 급여 조회
SELECT
       EMP_NAME
     , DEPT_CODE
     , SALARY
  FROM EMPLOYEE
 WHERE DEPT_CODE = 'D6'
   AND SALARY > 3000000;

-- DESC 키워드로 해당 테이블의 컬럼 및 자료형 정도를 손쉽게 확인할 수 있다.
DESC EMPLOYEE;

-- 보너스를 받지 않는 사원에 대해
-- 사번, 직원명, 급여, 보너스를 조회
SELECT * FROM EMPLOYEE; -- 보너스와 관련된 데이터 확인

DESC EMPLOYEE;

-- NULL과 관련된 WHERE절을 쓸 때는 IS NULL 또는 IS NOT NULL을 사용해야 한다.
SELECT
       EMP_ID
     , EMP_NAME
     , SALARY
     , BONUS
  FROM EMPLOYEE
-- WHERE BONUS IS NULL;     -- 보너스를 안 받는 경우
 WHERE BONUS IS NOT NULL;   -- 보너스를 받는 경우

-- 연결 연산자(||)를 이용하여 여러 컬럼을 하나의 컬럼인 것처럼 연결해서 RESULTSET으로 나타낼 수 있다.
SELECT
       EMP_ID || '사원의 이름은' || EMP_NAME || '이고 월급(급여)는 ' || SALARY "한방에 보는 컬럼"
  FROM EMPLOYEE;

SELECT
       EMP_NAME || '의 월급은' || SALARY || '원 입니다.' "사원별 월급"
  FROM EMPLOYEE;

-- 비교 연산자
-- = 같냐, > 크냐, < 작냐, >= 크거나 같냐, <= 작거나 같냐 (왼쪽을 기준으로 설명)
-- !=, ^=, <> 같지 않냐
 
-- D9부서에서 근무하지 않는 사원의
-- 사번, 이름, 부서코드를 조회
SELECT
       EMP_ID
     , EMP_NAME
     , DEPT_CODE
  FROM EMPLOYEE
-- WHERE DEPT_CODE != 'D9';
-- WHERE DEPT_CODE ^= 'D9';
 WHERE DEPT_CODE <> 'D9';

-- 풀어보기
-- EMPLOYEE 테이블에서 퇴사 여부가 N인 직원을 조회하고
-- 사번, 이름, 입사일을 별칭을 사용해 조회해 보기
-- (퇴사 여부 컬럼은 ENT_YN이고 N은 퇴사 안한 사람, Y는 퇴사 한 사람)
SELECT
       EMP_ID "사번"
     , EMP_NAME "이름"
     , HIRE_DATE "입사일"
  FROM EMPLOYEE
 WHERE ENT_YN = 'N';
  
DESC EMPLOYEE;

-- EMPLOYEE 테이블에서 급여를 350만원 이상
-- 550만원 이하를 받는
-- 직원의 사번, 이름, 급여, 부서코드, 직급코드를 별칭을 사용해 조회해 보기
SELECT
       EMP_ID "사번"
     , EMP_NAME "이름"
     , SALARY "급여"
     , DEPT_CODE "부서코드"
     , JOB_CODE "직급코드"
  FROM EMPLOYEE
-- WHERE SALARY >= 3500000
--   AND SALARY <= 5500000;
 WHERE SALARY BETWEEN 3500000 AND 5500000;   -- 이상, 이하 개념일 때만 쓰고 초과, 미만의 개념으로는 쓸 수 없다.

SELECT
       EMP_ID "사번"
     , EMP_NAME "이름"
     , SALARY "급여"
     , DEPT_CODE "부서코드"
     , JOB_CODE "직급코드"
  FROM EMPLOYEE
-- WHERE SALARY < 3500000
--    OR SALARY > 5500000;
-- WHERE SALARY NOT BETWEEN 3500000 AND 5500000;   
 WHERE NOT SALARY BETWEEN 3500000 AND 5500000; 

-- 컬럼값 BETWEEN 하한값 AND 상한값
-- : 하한값 이상, 상한값 이하의 값
-- 반대의 의미를 주는 NOT은 BETWEEN 앞 또는 컬럼명 앞에 붙일 수 있다.

-- LIKE 연산자: 문자 패턴이 일치하는 값을 조회할 때 사용
-- 컬럼명 LIKE '문자패턴'
-- 문자패턴: '글자%' (글자로 시작하는 값),
--          '%글자%' (글자가 포함된 값),
--          '%글자' (글자로 끝나는 값)
-- 와일드카드: _(문자 한자리), %(0개 이상의 글자)

-- EMPLOYEE 테이블에서 성이 김씨인 직원의
-- 사번, 이름, 입사일 조회
SELECT
       EMP_ID
     , EMP_NAME
     , HIRE_DATE
  FROM EMPLOYEE
 WHERE EMP_NAME LIKE '김%';

-- 성이 김씨가 아닌 직원
SELECT
       EMP_ID
     , EMP_NAME
     , HIRE_DATE
  FROM EMPLOYEE
-- WHERE EMP_NAME NOT LIKE '김%';
 WHERE NOT EMP_NAME LIKE '김%';

-- EMPLOYEE 테이블에서 '하'문자가 이름에 포함 된
-- 직원의 이름, 주민번호, 부서코드 조회
SELECT
       EMP_NAME
     , EMP_NO
     , DEPT_CODE
  FROM EMPLOYEE
 WHERE EMP_NAME LIKE '%하%';
 
-- EMPLOYEE 테이블에서 전화번호 국번이 3자리이면서 9로 시작하는
-- 직원의 사번, 이름, 전화번호를 조회
SELECT
       EMP_ID
     , EMP_NAME
     , PHONE
  FROM EMPLOYEE
 WHERE PHONE LIKE '___9%';
 
-- 국번이 3자리이면서 9로 시작하며 뒤에 7개의 번호가 있는 전화번호만 조회
SELECT
       EMP_ID
     , EMP_NAME
     , PHONE
  FROM EMPLOYEE
 WHERE PHONE LIKE '___9_______';
 
-- 데이터의 값 중에 와일드 카드와 구분하기 위해 ESCAPE '특수기호'를 쓸 수 있다.
-- EMPLOYEE 테이블에서 '_'앞글자가 3자리인 이메일 주소를 가진
-- 사원의 사번, 이름, 이메일 주소 조회
SELECT
       EMP_ID
     , EMP_NAME
     , EMAIL
  FROM EMPLOYEE
 WHERE EMAIL LIKE '___#_%' ESCAPE '#';
 
-- IN 연산자: 비교하려는 값 목록에 일치하는 값이 하나라도 있는지 확인하기 위한 연산자
-- D6부서 이거나 D8부서 이거나 D9부서인 직원들의 이름, 부서코드, 급여를 조회 
SELECT
       EMP_NAME
     , DEPT_CODE
     , SALARY
  FROM EMPLOYEE
-- WHERE DEPT_CODE = 'D6'
--    OR DEPT_CODE = 'D8'
--    OR DEPT_CODE = 'D9';
 WHERE DEPT_CODE IN ('D6', 'D8', 'D9');

-- 연산자 우선순위
/*
  1. 산술 연산자
  2. 연결 연산자(||)
  3. 비교 연산자
  4. IS NULL/IS NOT NULL, LIKE/NOT LIKE, IN/NOT IN
  5. BETWEEN AND/NOT BETWEEN AND
  6. NOT
  7. AND
  8. OR
*/

-- 'J2'직급이면서 급여가 200만원 이상인 직원이거나
-- 'J7'직급인 직원의 이름, 급여, 직급코드 조회
SELECT
       EMP_NAME
     , SALARY
     , JOB_CODE
  FROM EMPLOYEE
 WHERE JOB_CODE = 'J2'
   AND SALARY >= 2000000
    OR JOB_CODE = 'J7';
 
-- 'J2'직급이거나 'J7'직급인 직원들 중에
-- 급여가 200만원 이상인 직원의 이름, 급여, 직급코드 조회
SELECT
       EMP_NAME
     , SALARY
     , JOB_CODE
  FROM EMPLOYEE
 WHERE (JOB_CODE = 'J2'
    OR JOB_CODE = 'J7')
   AND SALARY >= 2000000;

-- IN 연산자로 업그레이드
SELECT
       EMP_NAME
     , SALARY
     , JOB_CODE
  FROM EMPLOYEE
 WHERE JOB_CODE IN ('J2', 'J7')
   AND SALARY >= 2000000;













