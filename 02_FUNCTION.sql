-- 그룹 함수와 단일행 함수
-- 함수(FUNCTION): 컬럼 값을 읽어서 계산한 결과를 리턴함
 
-- 그룹 함수(1행 이상, RESULT SET의 결과는 그룹별로 1행): SUM, AVG, MAX, MIN, COUNT
 
-- SUM(숫자가 기록 된 컬럼명): 합계를 구하여 리턴
-- 사원들의 급여 총합을 조회하시오.
SELECT
       SUM(SALARY)
  FROM EMPLOYEE;
  
-- AVG(숫자가 기록 된 컬럼명): 평균을 구하여 리턴
-- 사원들의 급여의 평균을 조회
SELECT
       AVG(SALARY)
  FROM EMPLOYEE;
  
-- MIN(컬럼명): 컬럼에서 가장 작은 값 리턴
SELECT
       MIN(SALARY)
     , MIN(EMAIL)
     , MIN(HIRE_DATE)
  FROM EMPLOYEE;

-- MAX(컬럼명): 컬럼에서 가장 큰 값 리턴
SELECT
       MAX(SALARY)
     , MAX(EMAIL)
     , MAX(HIRE_DATE)
  FROM EMPLOYEE;
  
-- AVG(BONUS): 보너스를 받는 사람들만 고려해 평균을 낸 것(BONUS가 NULL인 행은 고려하지 않음)
-- AVG(DISTINCT BONUS): 같은 보너스를 받는 사람을 한명으로 취급해서 평균을 낸 것
-- AVG(NVL(BONUS, 0)): 모든 사람들을 고려해 보너스에 대한 평균을 낸 것
SELECT
       AVG(BONUS) "기본 평균"
     , AVG(DISTINCT BONUS) "중복 제거 평균"
     , AVG(NVL(BONUS, 0)) "NULL값 포함 평균"
  FROM EMPLOYEE;

-- DISTINCT: 중복값을 제거하고 종류를 확인할 수 있음(NULL도 하나의 종류로 취급)
SELECT
       DISTINCT BONUS
  FROM EMPLOYEE;
  
-- NVL(컬럼명, NULL을 대체할 값): NULL값을 대체하기 위한 함수(이건 단일행 함수!!)
SELECT
       NVL(BONUS, 0)
  FROM EMPLOYEE;

-- COUNT(*|컬럼명): 행(튜플)의 갯수를 헤아려서 리턴
SELECT  
       COUNT(*)                   -- NULL에 상관없이 전체 카운팅
     , COUNT(DEPT_CODE)           -- DEPT_CODE 컬럼의 값이 NULL이 아닌 경우만 카운팅, 부서를 배치 받은 사람들의 행의 갯수
     , COUNT(DISTINCT DEPT_CODE)  -- 사원들이 일하는 부서의 갯수(NULL은 부서의 종류로 취급 안함)
  FROM EMPLOYEE;

SELECT
       *
  FROM EMPLOYEE
 WHERE DEPT_CODE IS NULL;
 
SELECT
       DISTINCT DEPT_CODE
  FROM EMPLOYEE;
  
SELECT
       *
  FROM DEPARTMENT;
 
-- 단일행 함수
---------------------------------------------------------------------------------
-- 문자 관련 함수
-- LENGTH, LEGNTHB, SUBSTR, UPPER, LOWER, INSTR, ...
SELECT
       LENGTH('오라클')
     , LENGTHB('오라클')
  FROM DUAL;

SELECT
       1 + 2
     , SYSDATE
  FROM DUAL;
  
SELECT
       EMP_ID
     , EMP_NAME
     , LENGTH(EMP_NAME)
  FROM EMPLOYEE;

-- INSTR('문자열' | 컬럼명, '문자', 찾을 위치의 시작값, [빈도])
SELECT INSTR('AABAACAABBAA', 'B') FROM DUAL;
SELECT INSTR('AABAACAABBAA', 'B', 4) FROM DUAL;
SELECT INSTR('AABAACAABBAA', 'B', -3) FROM DUAL;
SELECT INSTR('AABAACAABBAA', 'B', 1, 2) FROM DUAL;
SELECT INSTR('AABAACAABBAA', 'B', -1, 2) FROM DUAL;

SELECT
       EMAIL
     , INSTR(EMAIL, '@')    -- 각 행마다 '@'의 위치값 반환
  FROM EMPLOYEE;
  
-- SUBSTR: 시작 위치와 종료 위치를 토대로 컬럼 값(문자)을 뽑아낼 수 있다.
-- 이메일에서 @앞에 적는 아이디만 추출해 보기
SELECT
       EMAIL
     , SUBSTR(EMAIL, 1, 2)
     , SUBSTR(EMAIL, 1, INSTR(EMAIL, '@') - 1)  -- SUBSTR(컬럼명, 시작 위치, 길이)
     , INSTR(EMAIL, '@')
  FROM EMPLOYEE;

SELECT
       SUBSTR(EMP_NAME, 2, 1)
  FROM EMPLOYEE;

-- LTRIM / RTRIM: 
SELECT LTRIM('         THE') FROM DUAL;
SELECT LTRIM('         THE', ' ') FROM DUAL;
SELECT LTRIM('000123056', '0') FROM DUAL;
SELECT LTRIM('123321THE', '123') FROM DUAL;
SELECT LTRIM('123321THE123', '123') FROM DUAL;
SELECT LTRIM('ACABACCTHE', 'ABC') FROM DUAL;

SELECT RTRIM('THE         ') FROM DUAL;
SELECT RTRIM('THE         ', ' ') FROM DUAL;
SELECT RTRIM('123056000', '0') FROM DUAL;
SELECT RTRIM('THE123321', '123') FROM DUAL;
SELECT RTRIM('123THE123321', '123') FROM DUAL;
SELECT RTRIM('THEACABACC', 'ABC') FROM DUAL;

-- TRIM: 주어진 컬럼이나 문자열의 앞/뒤에 지정한 문자를 제거
SELECT TRIM('   THE   ') FROM DUAL;
SELECT TRIM('Z' FROM 'ZZZ123456ZZZ') FROM DUAL;
SELECT TRIM(LEADING 'Z' FROM 'ZZZTHEZZZ') FROM DUAL;        -- LTRIM과 같다.
SELECT TRIM(TRAILING '3' FROM '333THE333') FROM DUAL;       -- RTRIM과 같다.
SELECT TRIM(BOTH '3' FROM '333THE333') FROM DUAL;
  
-- LPAD / RPAD: 주어진 컬럼 문자열에 임의의 문자열을 덧붙여 길이가
--              N으로 동일한 문자열을 반환하는 함수

-- 우측 정렬의 개념을 줄 수도 있다.
SELECT
       LPAD(EMAIL, 20, ' ')
  FROM EMPLOYEE;
  
SELECT
       RPAD(EMAIL, 20, ' ')
  FROM EMPLOYEE;
  
-- LOWER/UPPER/INITCAP: 대소문자 변경 함수
SELECT
       LOWER('Welcome to My World')
  FROM DUAL;
  
SELECT
       UPPER('Welcome to My World')
  FROM DUAL;
  
SELECT
       INITCAP('welcome to my world')
  FROM DUAL;
  
-- CONCAT: 문자열 하나로 합하기
SELECT
       CONCAT(EMP_NAME, EMAIL)
  FROM EMPLOYEE;
  
SELECT
       EMP_NAME || EMAIL
  FROM EMPLOYEE;
  
-- REPLACE: 컬럼 혹은 문자열을 입력 받아
--          변경하고자 하는 문자열로 바꾼 후 반환
SELECT
       REPLACE('서울시 강남구', '강남구', '서초구')
  FROM DUAL;

-- 함수들 응용하기
-- EMPLOYEE 테이블에서 직원들의 주민번호를 조회하여
-- 사원명, 생년, 생월, 생일을 각각 분리하여 조회하시오.
-- 단, 컬럼의 별칭은 사원명, 생년, 생월, 생일로 한다.
SELECT
       EMP_NAME "사원명"
     , EMP_NO "주민등록번호"
     , SUBSTR(EMP_NO, 1, 2) "생년"
     , SUBSTR(EMP_NO, 3, 2) "생월"
     , SUBSTR(EMP_NO, 5, 2) "생일"
  FROM EMPLOYEE;

SELECT * FROM EMPLOYEE;
SELECT
       SUBSTR(EMP_NO, 5, 2)
  FROM EMPLOYEE;
 
-- 여직원들을 조회해 보자.(조회 컬럼은 알아서)
SELECT
       EMP_ID
     , EMP_NAME
     , '여' "성별"
  FROM EMPLOYEE
 WHERE SUBSTR(EMP_NO, 8, 1) = '2';

SELECT
       EMP_ID
     , EMP_NAME
     , '남' "성별"
  FROM EMPLOYEE
 WHERE SUBSTR(EMP_NO, 8, 1) = '1';

SELECT
       SUBSTR(EMP_NO, 8, 1)
  FROM EMPLOYEE;
 
-- EMPLOYEE 테이블에서 사원명, 주민번호 조회
-- (단, 주민 번호는 생년월일만 보이게 하고, '-'다음의 값은 '*'로 바꿔서 출력하시오.)
SELECT
       EMP_NAME
     , SUBSTR(EMP_NO, 1, 7) || '*******'
     , CONCAT(SUBSTR(EMP_NO, 1, 7), '*******')
  FROM EMPLOYEE;

--------------------------------------------------------------------------------------
-- 숫자 처리 함수: ABS, MOD, ROUND, FLOOR, TRUNC, CEIL
-- ABS: 절대값 구하는 함수
SELECT
       ABS(-10)         
     , ABS(10)          
  FROM DUAL;
  
-- MOD: 두 인수를 나누어서 나머지를 구하는 함수
--      처음 인자는 나누어지는 수, 두번째 인자는 나눌 수
SELECT
       MOD(10, 5)
     , MOD(10, 3)
  FROM DUAL;
  
-- ROUND: 반올림해서 리턴하는 함수
SELECT ROUND(123.456) FROM DUAL;
SELECT ROUND(123.456, 0) FROM DUAL;
SELECT ROUND(123.456, 1) FROM DUAL;
SELECT ROUND(123.456, 2) FROM DUAL;
SELECT ROUND(123.456, -1) FROM DUAL;

-- FLOOR: 내림처리 하는 함수
SELECT FLOOR(123.456) FROM DUAL;
SELECT FLOOR(123.678) FROM DUAL;
SELECT FLOOR(123.678 * 10) / 10 FROM DUAL;  -- 소수점 이하 한자리까지 표현
--SELECT FLOOR(123.678, 1) FROM DUAL;       -- 인수(argument) 갯수 틀려서 에러남

-- TRUNC: 내림처리(해당 위치부터 절삭)하는 함수
SELECT TRUNC(123.456) FROM DUAL;
SELECT TRUNC(123.456, 1) FROM DUAL;
SELECT TRUNC(123.456, 2) FROM DUAL;
SELECT TRUNC(123.456, -1) FROM DUAL;
 
-- CEIL: 올림처리 하는 함수
SELECT CEIL(123) FROM DUAL;
SELECT CEIL(123.456) FROM DUAL;
SELECT CEIL(123.678) FROM DUAL;

----------------------------------------------------------------------------------
-- 날짜 처리 함수: SYSDATE, MONTHS_BETWEEN, ADD_MONTH, NEXT_DAY, LAST_DAY, EXTRACT

-- SYSDATE: 시스템 현재 시간
SELECT SYSDATE FROM DUAL;
 
-- MONTHS_BETWEEN: 두 날짜의 개월 수 차이를 숫자로 리턴하는 함수(결과를 숫자 단일행 함수에 적용할 수 있다.)
SELECT
       EMP_NAME
     , HIRE_DATE
     , FLOOR(MONTHS_BETWEEN(SYSDATE, HIRE_DATE)) "한달 꽉 채워 일한 달 수"    -- 일한 날 중에 꽉 채워 일한 달 수만 계산
  FROM EMPLOYEE;

-- ADD_MONTHS: 날짜에 숫자만큼의 개월 수를 더해서 리턴하는 함수(달 별로 몇 일까지인지를 파악해서 개월 수를 더해 준다.(일이 동일하도록))
SELECT
       ADD_MONTHS(SYSDATE, 5)
  FROM DUAL;

-- EMPLOYEE 테이블에서 사원의 이름, 입사일, 입사 후 6개월이 되는 날짜를 조회
SELECT
       EMP_NAME
     , HIRE_DATE
     , ADD_MONTHS(HIRE_DATE, 5)
  FROM EMPLOYEE;

-- EMPLOYEE 테이블에서 근무 년수가 20년 이상인 직원의 사원의 이름, 입사일, 근무년수 조회
-- (단, 한달 꽉 채워 일한 달까지만 계산)
SELECT
       EMP_NAME "이름"
     , HIRE_DATE "입사일"
     , FLOOR(MONTHS_BETWEEN(SYSDATE, HIRE_DATE)) / 12
  FROM EMPLOYEE
-- WHERE FLOOR(MONTHS_BETWEEN(SYSDATE, HIRE_DATE)) / 12 >= 20   -- 숫자끼리 대소 비교
 WHERE ADD_MONTHS(HIRE_DATE, 240) <= SYSDATE;                   -- 날짜끼리 대소 비교
 
-- NEXT_DAY: 다음번 해당 요일을 날짜형으로 반환
SELECT SYSDATE, NEXT_DAY(SYSDATE, '목요일') FROM DUAL;
SELECT SYSDATE, NEXT_DAY(SYSDATE, 5) FROM DUAL;
SELECT SYSDATE, NEXT_DAY(SYSDATE, '목') FROM DUAL;
SELECT SYSDATE, NEXT_DAY(SYSDATE, 'THURSDAY') FROM DUAL;    -- 언어 설정을 바꿔야 동작함

-- ORACLE 언어 설정 변경
ALTER SESSION SET NLS_LANGUAGE = AMERICAN; -- 영어
ALTER SESSION SET NLS_LANGUAGE = KOREAN;    -- 한국어

-- LAST_DAY: 해당 날짜의 월에서 마지막 날짜를 구하여 리턴
SELECT
       SYSDATE
     , LAST_DAY(SYSDATE)
     , LAST_DAY('1987/02/02')
  FROM DUAL;

-- EMPLOYEE 테이블에서 사원명, 입사일, 입사한 달의 근무일 수를 조회
SELECT
       EMP_NAME
     , HIRE_DATE
     , LAST_DAY(HIRE_DATE) - HIRE_DATE + 1  "입사한 달의 근무일 수"  -- 날짜끼리 빼기 가능(일 수 차이가 숫자로), 큰 수가 더 최근
     , ABS(HIRE_DATE - LAST_DAY(HIRE_DATE)) + 1
  FROM EMPLOYEE;

-- EXTRACT: DATE형에서 년, 월, 일 정보를 추출하여 리턴하는 함수
SELECT
       EXTRACT(YEAR FROM SYSDATE) "올해"
     , EXTRACT(MONTH FROM SYSDATE) "이번 달"
     , EXTRACT(DAY FROM SYSDATE) "오늘 일수"
  FROM DUAL;
  
SELECT
       EMP_NAME
     , HIRE_DATE
     , EXTRACT(YEAR FROM HIRE_DATE) || '년' "입사년도"
     , EXTRACT(MONTH FROM HIRE_DATE) || '월' "입사월"
     , EXTRACT(DAY FROM HIRE_DATE) || '일' "입사일"
  FROM EMPLOYEE;
  
-------------------------------------------------------------------------------------
-- 형변환 함수
-- TO_CHAR(날짜, [포맷]): 날짜형 데이터를 문자형 데이터로 변경
-- TO_CHAR(숫자, [포맷]): 숫자형 데이터를 문자형 데이터로 변경

-- NUMBER -> CHAR
SELECT TO_CHAR(1234) FROM DUAL;
SELECT TO_CHAR(1234, '99999') FROM DUAL;
SELECT TO_CHAR(1234, '00000') FROM DUAL;
SELECT TO_CHAR(1234, 'L99999') FROM DUAL;
SELECT TO_CHAR(1234, '$99999') FROM DUAL;
SELECT TO_CHAR(1234, 'L999,999,999') FROM DUAL;
SELECT TO_CHAR(1234, 'L000,000,000') FROM DUAL;
SELECT TO_CHAR(1234, '9.9EEEE') FROM DUAL;      -- 지수 표현으로 나타낼 수 있음
SELECT TO_CHAR(1234, '999') FROM DUAL;          -- 형변환 할 숫자보다 적은 포맷을 작성하면 '#'으로 처리됨

-- 직원 테이블에서 사원명, 급여 조회
-- (단, 급여는 '￦9,000,000' 형식으로 조회)
SELECT
       EMP_NAME
     , TO_CHAR(SALARY, 'L999,999,999')
  FROM EMPLOYEE;
  
-- 날짜 데이터도 포맷 적용해서 문자형으로 변환할 수 있다.
SELECT TO_CHAR(SYSDATE, 'PM HH24:MI:SS') FROM DUAL;
SELECT TO_CHAR(SYSDATE, 'AM HH:MI:SS') FROM DUAL;
SELECT TO_CHAR(SYSDATE, 'MON DY, YYYY') FROM DUAL;
SELECT TO_CHAR(SYSDATE, 'YYYY-fmMM-DD DAY') FROM DUAL;
SELECT TO_CHAR(SYSDATE, 'YYYY-MM-DD DAY') FROM DUAL;
SELECT TO_CHAR(SYSDATE, 'YEAR, Q') || '분기' FROM DUAL;
 
SELECT
       EMP_NAME
     , HIRE_DATE
     , TO_CHAR(HIRE_DATE, 'YYYY-MM-DD') "입사일"
     , TO_CHAR(HIRE_DATE, 'YYYY-MM-DD HH24:MI:SS') "상세 입사일"
  FROM EMPLOYEE;
 
-- 날짜에 대해 4자리 년도, 2자리 년도 이름으로 조회해 보기
SELECT
       TO_CHAR(SYSDATE, 'YYYY')
     , TO_CHAR(SYSDATE, 'RRRR')
     , TO_CHAR(SYSDATE, 'YY')
     , TO_CHAR(SYSDATE, 'RR')
     , TO_CHAR(SYSDATE, 'YEAR')
     , TO_CHAR(SYSDATE, 'YYYY-MM-DD HH24:MI:SS')
  FROM DUAL;

-- TO_DATE: CHAR형을 DATE형으로 변환하는 함수
-- RR과 YY의 차이
SELECT
       TO_CHAR(TO_DATE('980630', 'YYMMDD'), 'YYYY-MM-DD')
  FROM DUAL;
 
SELECT
       TO_CHAR(TO_DATE('980630', 'RRMMDD'), 'YYYY-MM-DD')
  FROM DUAL;  
  
-- RR은 두자리 값으로 년도를 인식할 때
-- 바꿀 년도가 50 미만(00~49)이면 2000년대를 적용하고
-- 50 이상(50~99)이면 1900년대를 적용한다.

-- 입사일을 '2018년 6월 10일 (수)'같은 형식으로 처리해 보기
SELECT
       EMP_NAME
     , TO_CHAR(HIRE_DATE, 'RRRR"년" fmMM"월" DD"일" (DY)')
  FROM EMPLOYEE;
  
-- 배운걸 토대로 약간의 응용
SELECT
       '123' + '10'                            -- 비추천(자동형변환 됨)
     , TO_NUMBER('123') + TO_NUMBER('10')      -- 추천
  FROM DUAL;
  
-- 사원 테이블에서 2000년도 이후에 입사한 사원의
-- 사번, 이름, 입사일을 조회
SELECT
       EMP_ID
     , EMP_NAME
     , HIRE_DATE
  FROM EMPLOYEE
-- WHERE HIRE_DATE >= TO_DATE('20000101', 'RRRRMMDD');  -- 추천
-- WHERE EXTRACT(YEAR FROM HIRE_DATE) >= 2000;          -- 추천
 WHERE HIRE_DATE >= '20000101';                         -- 자동 형변환이 되는건 비추천

-----------------------------------------------------------------------------------------
-- 선택함수
-- 여러 가지 경우에 선택할 수 있는 기능을 제공한다.
-- DECODE(계산식|컬럼명, 조건값1, 선택값1, 조건값2, 선택값2...)
-- 자바랑 비교하면 한줄 SWITCH문과 같은 개념이다.
SELECT
       EMP_ID
     , EMP_NAME
     , EMP_NO
     , DECODE(SUBSTR(EMP_NO, 8, 1), '1', '남', '2', '여') "성별"
  FROM EMPLOYEE;

-- 마지막 인수로 조건값이 없이 선택값만 작성하면
-- 어떤 조건 값에도 해당하지 않는 경우 마지막 선택 값으로 결정된다.
-- 자바의 else나 switch의 default 같은 개념
SELECT
       EMP_ID
     , EMP_NAME
     , EMP_NO
     , DECODE(SUBSTR(EMP_NO, 8, 1), '1', '남', '여') "성별"
  FROM EMPLOYEE;

-- 직원의 급여를 인상하고자 한다.
-- 직급 코드가 'J7'인 직원은 급여의 10%를 인상하고
-- 직급 코드가 'J6'인 직원은 급여의 15%를 인상하고
-- 직급 코드가 'J5'인 직원은 급여의 20%를 인상한다.
-- 그 외 직급의 직원은 5%만 인상한다.
-- 위의 조건을 만족하며 직원 테이블에서
-- 직원명, 직급코드, 급여, 인상급여를 조회
 SELECT
        EMP_NAME "직원명"
      , JOB_CODE "직급코드"
      , SALARY "급여"
      , DECODE(JOB_CODE, 'J7', SALARY * 1.1
                       , 'J6', SALARY * 1.15
                       , 'J5', SALARY * 1.2
                             , SALARY * 1.05) "인상급여"
   FROM EMPLOYEE;
   
-- CASE
--  WHEN 조건식1 THEN 결과값1
--  WHEN 조건식2 THEN 결과값2
--  ELSE 결과값3
-- END
SELECT
       EMP_NAME "직원명"
     , JOB_CODE "직급코드"
     , SALARY "급여"
     , CASE
        WHEN JOB_CODE = 'J7' THEN SALARY * 1.1
        WHEN JOB_CODE = 'J6' THEN SALARY * 1.15
        WHEN JOB_CODE = 'J5' THEN SALARY * 1.2
        ELSE SALARY * 1.05
       END "인상급여"
  FROM EMPLOYEE;

-- 사번, 사원명, 급여를 EMPLOYEE 테이블에서 조회하고
-- 급여가 500만원 초과이면 '고급'
-- 급여가 300만원 초과 ~ 500만원 이하이면 '중급'
-- 그 이하는 '초급'으로 출력 처리하고 별칭은 '구분'으로 한다.
SELECT
       EMP_ID "사번"
     , EMP_NAME "사원명"
     , SALARY "급여"
     , CASE
        WHEN SALARY > 5000000 THEN '고급'
        WHEN SALARY > 3000000 AND SALARY <= 5000000 THEN '중급'
        ELSE '초급'
       END "구분"
  FROM EMPLOYEE;
  
----------------------------------------------------------------------------------
-- 함수 연습 문제
--1. 부서코드가 D5, D9인 직원들 중에서 2004년도에 입사한 직원의 
--	사번 사원명 부서코드 입사일 조회
SELECT
       EMP_ID
     , EMP_NAME
     , DEPT_CODE
     , HIRE_DATE
  FROM EMPLOYEE
 WHERE DEPT_CODE IN ('D5', 'D9')
   AND EXTRACT(YEAR FROM HIRE_DATE) = 2004;
 
SELECT
       EXTRACT(YEAR FROM HIRE_DATE)
  FROM EMPLOYEE;

--2. 직원명, 부서코드, 생년월일, 나이(만) 조회
--  단, 생년월일은 주민번호에서 추출해서, 
--     ㅇㅇ년 ㅇㅇ월 ㅇㅇ일로 출력되게 함.
--  나이는 주민번호에서 추출해서 날짜데이터로 변환한 다음, 계산함
SELECT
       EMP_NAME "직원명"
     , DEPT_CODE "부서코드"
--     , TO_CHAR(TO_DATE(SUBSTR(EMP_NO, 1, 6), 'RRMMDD'), 'RRRR"년" MM"월" DD"일"') "생년월일"
     , SUBSTR(EMP_NO, 1, 2) || '년 ' ||
       SUBSTR(EMP_NO, 3, 2) || '월 ' ||
       SUBSTR(EMP_NO, 5, 2) || '일'
     , FLOOR(MONTHS_BETWEEN(SYSDATE, TO_DATE(SUBSTR(EMP_NO, 1, 6), 'RRMMDD')) / 12) "나이(만)"
  FROM EMPLOYEE
 WHERE TO_NUMBER(SUBSTR(EMP_NO, 5, 2)) < 32;    

--3.  부서코드가 D5이면 총무부, D6이면 기획부, D9이면 영업부로 처리하시오.
--   단, 부서코드가 D5, D6, D9 인 직원의 정보만 조회함
--  => CASE문 사용
SELECT
       EMP_NAME
     , CASE
        WHEN DEPT_CODE = 'D5' THEN '총무부'
        WHEN DEPT_CODE = 'D6' THEN '기획부'
        WHEN DEPT_CODE = 'D9' THEN '영업부'
       END
  FROM EMPLOYEE
 WHERE DEPT_CODE IN ('D5', 'D6', 'D9');
  
  
  
  
  
  
  
  