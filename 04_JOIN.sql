-- 조인(JOIN)
-- JOIN: 한 개 이상의 테이블을 하나로 합쳐서 하나의 결과(RESULT SET)으로 조회하기 위해 사용된다.
 
-- 오라클 전용
-- 연결에 사용 할 두 컬럼의 컬럼명이 다른 경우
SELECT
       EMP_ID
     , EMP_NAME
     , DEPT_CODE
     , DEPT_TITLE
     , LOCATION_ID
  FROM EMPLOYEE
     , DEPARTMENT
 WHERE DEPT_CODE = DEPT_ID;
 
SELECT * FROM EMPLOYEE;
SELECT * FROM DEPARTMENT;

-- 연결에 사용할 두 컬럼의 컬럼명이 같은 경우
SELECT
       EMP_ID
     , EMP_NAME
--     , EMPLOYEE.JOB_CODE
--     , JOB.JOB_CODE
     , JOB_NAME
  FROM EMPLOYEE
     , JOB
 WHERE EMPLOYEE.JOB_CODE = JOB.JOB_CODE;
 
SELECT * FROM JOB;  
 
-- ANSI 표준★★★
-- 연결에 사용 할 두 컬럼의 컬럼명이 다른 경우
SELECT
       EMP_ID
     , EMP_NAME
     , DEPT_CODE
     , DEPT_TITLE
     , LOCATION_ID
  FROM EMPLOYEE
  JOIN DEPARTMENT ON (DEPT_CODE = DEPT_ID);

-- 연결에 사용할 두 컬럼의 컬럼명이 같은 경우
SELECT
       EMP_ID
     , EMP_NAME
     , EMPLOYEE.JOB_CODE
     , JOB_NAME
  FROM EMPLOYEE
  JOIN JOB ON (EMPLOYEE.JOB_CODE = JOB.JOB_CODE);

SELECT
       EMP_ID
     , EMP_NAME
     , JOB_CODE
     , JOB_NAME
  FROM EMPLOYEE
  JOIN JOB USING (JOB_CODE);
  
-- 오라클 방식과 ANSI 방식을 각각 별칭을 사용해 보자.
-- 오라클 방식
SELECT
       A.EMP_ID
     , A.EMP_NAME
     , A.JOB_CODE
     , B.JOB_NAME
  FROM EMPLOYEE A
     , JOB B
 WHERE A.JOB_CODE = B.JOB_CODE;

-- ANSI 방식
SELECT
       A.EMP_ID
     , A.EMP_NAME
     , A.JOB_CODE
     , B.JOB_NAME
  FROM EMPLOYEE A
  JOIN JOB B ON (A.JOB_CODE = B.JOB_CODE);
  
-- JOIN하는 테이블들의 관계 있는 컬럼의 컬럼명이 동일하든 동일하지 않든 별칭을 달아 조회하자.(가독성, 유지보수)

-- 조인은 기본이 EQUAL JOIN이다.(EQU JOIN()이라고도 함)
-- 연결되는 튜플의 컬럼 값이 일치하는 행들만 조인됨
 
-- 일치하는 값이 없는 행은 조인에서 제외되는 것을 INNER JOIN이라고 한다.
 
-- JOIN의 기본은 INNER JOIN & EQU JOIN이다.
 
-- OUTER JOIN: 두 테이블의 지정하는 컬럼 값이 일치하지 않는 행도
--             조인에 포함을 시킴
--             반드시 OUTER JOIN임을 명시해야 한다.
-- 1. LEFT OUTER JOIN: 합치기에 사용한 두 테이블 중 왼편에 기술 된 테이블의 행의 수를 기준으로 JOIN
-- 2. RIGHT OUTER JOIN: 합치기에 사용한 두 테이블 중 오른편에 기술 된 테이블의 행의 수를 기준으로 JOIN
-- 3. FULL OUTER JOIN: 합치기에 사용한 두 테이블이 가진 모든 행을 결과에 포함하여 JOIN
 
-- ANSI표준의 INNER JOIN
SELECT
       A.EMP_NAME
     , B.DEPT_TITLE
  FROM EMPLOYEE A
-- INNER JOIN DEPARTMENT B ON (A.DEPT_CODE = B.DEPT_ID);
  JOIN DEPARTMENT B ON (A.DEPT_CODE = B.DEPT_ID);   -- 매핑 조건을 만족하지 못하는 행은 조회되지 않음

-- ANSI표준의 LEFT OUTER JOIN
SELECT
       A.EMP_NAME
     , B.DEPT_TITLE
  FROM EMPLOYEE A
  LEFT JOIN DEPARTMENT B ON (A.DEPT_CODE = B.DEPT_ID); -- 매핑 조건을 만족하지 않더라도 모든 사원 조회

-- ANSI표준의 RIGHT OUTER JOIN
SELECT
       A.EMP_NAME
     , B.DEPT_TITLE
  FROM EMPLOYEE A
 RIGHT JOIN DEPARTMENT B ON (A.DEPT_CODE = B.DEPT_ID); -- 매핑 조건을 만족하지 않더라도 모든 부서 조회
 
-- ORACLE 전용 LEFT OUTER JOIN
SELECT
       A.EMP_NAME
     , B.DEPT_TITLE
  FROM EMPLOYEE A
     , DEPARTMENT B
 WHERE A.DEPT_CODE = B.DEPT_ID(+);

-- ORACLE 전용 RIGHT OUTER JOIN
SELECT
       A.EMP_NAME
     , B.DEPT_TITLE
  FROM EMPLOYEE A
     , DEPARTMENT B
 WHERE A.DEPT_CODE(+) = B.DEPT_ID;
 
-- ANSI 전용 FULL OUTER JOIN
SELECT
       A.EMP_NAME
     , B.DEPT_TITLE
  FROM EMPLOYEE A
  FULL JOIN DEPARTMENT B ON (A.DEPT_CODE = B.DEPT_ID); -- 매핑 조건을 만족하지 않더라도 모든 사원, 모든 부서 조회

-- 오라클 방식에서는 FULL OUTER JOIN을 제공하지 않는다.(WHERE 컬럼 양쪽에 (+) 붙이면 에러남)

-- CROSS JOIN: 카테이션 곱이라고 한다.
--             조인되는 테이블들의 각 행들이 모두 매핑되게 데이터를 검색하는 방법이다.
SELECT
       A.EMP_NAME
     , B.DEPT_TITLE
  FROM EMPLOYEE A
 CROSS JOIN DEPARTMENT B;
 
-- JOIN할 때 ON의 조건을 잘못 쓰면 CROSS JOIN이 발생한다.(일반적으로 의도하지 않은 결과로 쿼리를 다시 확인한다.)
SELECT
       A.EMP_NAME
     , B.DEPT_TITLE
  FROM EMPLOYEE A
  JOIN DEPARTMENT B ON (1 = 1);

-- NON EQUAL JOIN(NON EQU JOIN)
SELECT * FROM EMPLOYEE;
SELECT * FROM SAL_GRADE;

-- EQU JOIN
SELECT
       A.EMP_NAME
     , B.MIN_SAL
     , B.MAX_SAL
  FROM EMPLOYEE A
  JOIN SAL_GRADE B ON (A.SAL_LEVEL = B.SAL_LEVEL);
  
-- NON EQU JOIN
SELECT
       A.EMP_NAME
     , A.SAL_LEVEL
     , B.MIN_SAL
     , B.MAX_SAL
  FROM EMPLOYEE A
--  JOIN SAL_GRADE B ON (A.SALARY >= B.MIN_SAL AND A.SALARY <= B.MAX_SAL);
  JOIN SAL_GRADE B ON (A.SALARY BETWEEN B.MIN_SAL AND B.MAX_SAL);

-- SELF JOIN: 같은 테이블을 조인하는 경우
--            자기 자신 테이블과 조인을 맺는 것이다.
SELECT * FROM EMPLOYEE;

-- ANSI 표준
SELECT
       A.EMP_ID
     , A.EMP_NAME "사원 이름"
     , A.DEPT_CODE
     , A.MANAGER_ID
     , B.EMP_NAME "관리자 이름"
  FROM EMPLOYEE A
  LEFT JOIN EMPLOYEE B ON (A.MANAGER_ID = B.EMP_ID);
  
-- ORACLE 표준
SELECT
       A.EMP_ID
     , A.EMP_NAME "사원 이름"
     , A.DEPT_CODE
     , A.MANAGER_ID
     , B.EMP_NAME "관리자 이름"
  FROM EMPLOYEE A
     , EMPLOYEE B
 WHERE A.MANAGER_ID = B.EMP_ID(+);
 
-- 다중 JOIN: N개(3개 이상)의 테이블을 조회할 때 사용하는 JOIN
-- ANSI 표준
SELECT
       A.EMP_ID
     , A.EMP_NAME
     , A.DEPT_CODE
     , B.DEPT_TITLE
     , C.LOCAL_NAME
     , C.NATIONAL_CODE
  FROM EMPLOYEE A
  LEFT JOIN DEPARTMENT B ON (A.DEPT_CODE = B.DEPT_ID)
  LEFT JOIN LOCATION C ON (B.LOCATION_ID = C.LOCAL_CODE)
 WHERE C.NATIONAL_CODE = 'KO'; -- 이전 조인에서 OUTER 조인을 했다면 OUTER 조인을 계속 적용해야 원하는 결과(사원 모두 조회)를 얻을 수 있다.

SELECT * FROM EMPLOYEE;
SELECT * FROM DEPARTMENT;
SELECT * FROM LOCATION;

-- ORACLE 표준
SELECT
       A.EMP_ID
     , A.EMP_NAME
     , A.DEPT_CODE
     , B.DEPT_TITLE
     , C.LOCAL_NAME
     , C.NATIONAL_CODE
  FROM EMPLOYEE A
     , DEPARTMENT B
     , LOCATION C
 WHERE A.DEPT_CODE = B.DEPT_ID(+)
   AND B.LOCATION_ID = C.LOCAL_CODE(+)
   AND C.NATIONAL_CODE = 'KO';

-- 직급이 대리이면서 아시아 지역(ASIA1, ASIA2, ASIA3 모두 해당)에 근무하는 직원 조회
-- 사번(EMPLOYEE.EMP_ID), 이름(EMPLOYEE.EMP_NAME), 직급명(JOB.JOB_NAME), 부서명(DEPARTMENT.DEPT_TITLE),
-- 근무지역명(LOCATION.LOCAL_NAME), 급여(EMPLOYEE.SALARY)를 조회하시오
-- (해당 컬럼을 찾고, 해당 컬럼을 지닌 테이블들을 찾고, 테이블들을 어떤 순서로 조인해야 하는지 고민하고 SQL문을 작성할 것)
SELECT
       A.EMP_ID
     , A.EMP_NAME
     , B.JOB_NAME
     , C.DEPT_TITLE
     , D.LOCAL_NAME
     , A.SALARY
  FROM EMPLOYEE A
  LEFT JOIN JOB B ON (A.JOB_CODE = B.JOB_CODE)
  LEFT JOIN DEPARTMENT C ON (A.DEPT_CODE = C.DEPT_ID)
  LEFT JOIN LOCATION D ON (C.LOCATION_ID = D.LOCAL_CODE)
 WHERE B.JOB_NAME = '대리'
--   AND D.LOCAL_NAME IN ('ASIA1', 'ASIA2', 'ASIA3');
   AND D.LOCAL_NAME LIKE 'ASIA%';
   
SELECT * FROM JOB;
SELECT * FROM DEPARTMENT;
SELECT * FROM LOCATION;

---------------------------------------------------------------------------------------------------
JOIN 연습문제

1. 2020년 12월 25일이 무슨 요일인지 조회하시오.(JOIN 문제는 아님)

 
2. 주민번호가 70년대 생이면서 성별이 여자이고, 성이 전씨인 직원들의 
사원명, 주민번호, 부서명, 직급명을 조회하시오.(1명)


3. 가장 나이가 적은 직원의 사번, 사원명, 나이, 부서명, 직급명을 조회하시오.(1명)
   (나이는 만 나이가 아니라 태어나면서부터 1살로 계산해 볼 것)



4. 이름에 '형'자가 들어가는 직원들의 사번, 사원명, 부서명을 조회하시오.(1명)



5. 해외영업팀에 근무하는 사원명, 직급명, 부서코드, 부서명을 조회하시오.(9명)



6. 보너스포인트를 받는 직원들의 사원명, 보너스포인트, 부서명, 근무지역명을 조회하시오.(8명)



7. 부서코드가 D2인 직원들의 사원명, 직급명, 부서명, 근무지역명을 조회하시오.(3명)



8. 급여 테이블의 등급별 최소급여(MIN_SAL)보다 많이 받는 직원들의
사원명, 직급명, 급여, 연봉을 조회하시오.
연봉에 보너스포인트를 적용하시오.(20명)



9. 한국(KO)과 일본(JP)에 근무하는 직원들의 
사원명, 부서명, 지역명, 국가명을 조회하시오.(15명)


10. 같은 부서에 근무하는 직원들의 사원명, 부서코드, 동료이름을 조회하시오.
self join 사용(60명)



11. 보너스포인트가 없는 직원들 중에서 직급코드가 J4와 J7인 직원들의 사원명, 직급명, 급여를 조회하시오.
단, join과 IN 사용할 것(8명)


12. 재직중인 직원과 퇴사한 직원의 수를 조회하시오.
 COUNT(*)
=========
   1
   22













