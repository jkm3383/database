-- DML(Data Manipulation Language)
-- INSERT, UPDATE, DELETE, SELECT(DQL)
-- : 데이터 조작언어, 테이블에 값을 삽입하거나, 수정하거나,
--   삭제하는 언어(조회하는 언어를 포함하기도 함)
 
-- INSERT: 새로운 행을 추가하는 구문이다.
--         테이블 행의 갯수가 증가한다.
-- INSERT INTO 테이블명(컬럼명1, 컬럼명2, ...) VALUES(데이터1, 데이터2, ...)

DESC EMPLOYEE;

INSERT
  INTO EMPLOYEE
(
  EMP_ID, EMP_NAME, EMP_NO
, EMAIL, PHONE, DEPT_CODE
, JOB_CODE, SAL_LEVEL, SALARY
, BONUS, MANAGER_ID, HIRE_DATE
, ENT_DATE, ENT_YN
)
VALUES
(
  900, '홍길동', '901123-1231234'
, 'hong@greedy.or.kr', '01012345555', 'D1'
, 'J1', 'S5', 2800000
, 0.1, '200', SYSDATE
, NULL, DEFAULT
);

COMMIT;
SELECT * FROM EMPLOYEE;

-- DEFAULT: 테이블 생성 시 DEFAULT와 관련된 설정을 컬럼에 부여할 수 있고
--          INSERT 시, DEFAULT라고 INSERT를 하게 되면 DEFAULT로 설정한 값이 INSERT되게 할 수 있다.
--          또한 INSERT 시 해당 컬럼에 대해서는 무시하고 INSERT를 하게 되면 DEFAULT로 설정한 값이 해당 컬럼에 들어가게 된다.
--          (해당 컬럼에 NULL을 넣으면 NULL값이 들어감)
DROP TABLE TEST_DEF;
CREATE TABLE TEST_DEF(
 C_FIRST NVARCHAR2(1) DEFAULT 'N' NOT NULL,
 C_SECOND NVARCHAR2(1),
 CHECK(C_FIRST IN('Y', 'N'))
);

-- C_FIRST 컬럼을 무시하고 C_SECOND만 고려해서 INSERT
-- (원래는 C_FIRST 컬럼엔 NULL이 들어가야 되지만 DEFAULT 설정 값이 들어가게 된다 -> NOT NULL과 CHECK 제약조건을 만족)
INSERT
  INTO TEST_DEF
(
  C_SECOND
)
VALUES
(
  NULL
);

SELECT * FROM TEST_DEF;

-- 컬럼을 제약조건에 위반되지 않게(NOT NULL) 선택해서 INSERT하기
INSERT
  INTO EMPLOYEE
(
  EMP_ID, EMP_NAME, EMP_NO       -- 나머지 컬럼은 기본적으로 NULL이 들어가고 DEFAULT 값이 설정된 컬럼은 DEFAULT 값이 들어간다.
, EMAIL, JOB_CODE, SAL_LEVEL
)
VALUES
(
  '901', '유관순', '901231-2123123'
, 'yu@greedy.or.kr', 'J1', 'S5'
);

SELECT
       A.*
  FROM EMPLOYEE A
 WHERE A.EMP_ID = '901';

COMMIT;

-- INSERT 시에 VALUES 대신 서브쿼리를 활용할 수도 있다.
CREATE TABLE EMP_01(
  EMP_ID VARCHAR2(3),
  EMP_NAME VARCHAR2(20),
  DEPT_TITLE VARCHAR2(35)
);

DESC EMPLOYEE;
DESC DEPARTMENT;

INSERT
  INTO EMP_01
(
  EMP_ID
, EMP_NAME
, DEPT_TITLE
)
(
  SELECT A.EMP_ID
       , A.EMP_NAME
       , B.DEPT_TITLE
    FROM EMPLOYEE A
    LEFT JOIN DEPARTMENT B ON (A.DEPT_CODE = B.DEPT_ID)
);

SELECT * FROM EMP_01;

-- INSERT ALL: INSERT 시에 사용하는 서브쿼리가 같은 경우
--             두 개 이상의 테이블에 INSERT ALL을 이용하여
--             한번에 데이터를 삽입할 수 있다.
--             단, 각 서브쿼리의 조건절이 같아야 한다.

-- 서브쿼리로 테이블 만들기(테이블 틀만 복사하는 개념)
DROP TABLE EMP_DEPT_D1;
CREATE TABLE EMP_DEPT_D1
AS
SELECT A.EMP_ID         -- 기존 테이블의 컬럼에 걸린 제약조건 중에는 NOT NULL만 복사되고 나머지 제약조건은 복사되지 않는다.
     , A.EMP_NAME
     , A.DEPT_CODE
     , A.HIRE_DATE
  FROM EMPLOYEE A
 WHERE 1 = 0;

SELECT * FROM EMP_DEPT_D1;

CREATE TABLE EMP_MANAGER
AS
SELECT A.EMP_ID 
     , A.EMP_NAME
     , A.MANAGER_ID
  FROM EMPLOYEE A
 WHERE 1 = 0;

SELECT * FROM EMP_MANAGER;

-- INSERT ALL을 활용해 하나의 쿼리로 두 테이블에 INSERT하기
INSERT ALL
  INTO EMP_DEPT_D1
VALUES
(
  EMP_ID, EMP_NAME
, DEPT_CODE, HIRE_DATE
)
  INTO EMP_MANAGER
VALUES
(
  EMP_ID, EMP_NAME
, MANAGER_ID
) 
(
  SELECT A.EMP_ID
       , A.EMP_NAME
       , A.DEPT_CODE
       , A.HIRE_DATE
       , A.MANAGER_ID
    FROM EMPLOYEE A
   WHERE A.DEPT_CODE = 'D1'
);

SELECT * FROM EMP_DEPT_D1;
SELECT * FROM EMP_MANAGER;

-- EMPLOYEE 테이블에서 입사일 기준으로 2000년 1월 1일 이전에 입사한
-- 사원의 사번, 이름, 입사일, 급여를 조회하여
-- EMP_OLD 테이블에 삽입하고
-- 그 이후에 입사한 사원은 EMP_NEW 테이블에 삽입하시오.
CREATE TABLE EMP_OLD
AS
SELECT A.EMP_ID
     , A.EMP_NAME
     , A.HIRE_DATE
     , A.SALARY
  FROM EMPLOYEE A
 WHERE 1 = 0;

CREATE TABLE EMP_NEW
AS
SELECT A.EMP_ID
     , A.EMP_NAME
     , A.HIRE_DATE
     , A.SALARY
  FROM EMPLOYEE A
 WHERE 1 = 0;

INSERT ALL
  WHEN HIRE_DATE < TO_DATE('2000/01/01', 'RRRR/MM/DD')
  THEN
  INTO EMP_OLD
VALUES
(
  EMP_ID
, EMP_NAME
, HIRE_DATE
, SALARY
)
  WHEN HIRE_DATE >= TO_DATE('2000/01/01', 'RRRR/MM/DD')
  THEN
  INTO EMP_NEW
VALUES
(
  EMP_ID
, EMP_NAME
, HIRE_DATE
, SALARY
)
SELECT
       A.EMP_ID
     , A.EMP_NAME
     , A.HIRE_DATE
     , A.SALARY
  FROM EMPLOYEE A;

SELECT * FROM EMP_OLD;
SELECT * FROM EMP_NEW;

-- UPDATE: 테이블에 기록된 컬럼의 값을 수정하는 구문이다.
--         테이블의 전체 행 갯수는 변화가 없다.
-- UPDATE 테이블명 SET 컬럼명1 = 바꿀값1, 컬럼명2 = 바꿀값2, ...
-- [WHERE 컬럼명 비교연산자 비교값]

-- DEPARTMENT 복사본 테이블 만들기
CREATE TABLE DEPT_COPY
AS
SELECT A.*
  FROM DEPARTMENT A;

SELECT * FROM DEPT_COPY;

-- 'D9'부서의 부서명을 '전략기획팀'으로 수정
UPDATE
       DEPT_COPY A
   SET A.DEPT_TITLE = '전략기획팀'
 WHERE A.DEPT_ID = 'D9';

ROLLBACK;
COMMIT;

-- UPDATE 시에도 서브쿼리를 이용할 수 있다.
-- UPDATE 테이블명
-- SET 컬럼명 = (서브쿼리)

-- 사원 복사본 테이블 만들기
CREATE TABLE EMP_SALARY
AS
SELECT A.EMP_ID
     , A.EMP_NAME
     , A.DEPT_CODE
     , A.SALARY
     , A.BONUS
  FROM EMPLOYEE A;

-- 평상 시에 유재식 사원을 부러워 하던 방명수 사원의
-- 급여와 보너스율을 유재식 사원과 동일하게 변경해 주기로 했다.
-- 이를 반영하는 UPDATE문을 작성하시오.
SELECT
       A.*
  FROM EMP_SALARY A
 WHERE A.EMP_NAME IN ('유재식', '방명수');

UPDATE
       EMP_SALARY A
   SET A.SALARY = (SELECT B.SALARY
                     FROM EMP_SALARY B
                    WHERE B.EMP_NAME = '유재식'
                  )
     , A.BONUS = (SELECT C.BONUS
                    FROM EMP_SALARY C
                   WHERE C.EMP_NAME = '유재식'
                 )
 WHERE A.EMP_NAME = '방명수';
 
-- 다중열 서브쿼리를 이용한 업데이트 문
UPDATE
       EMP_SALARY A
   SET (A.SALARY, A.BONUS) = (SELECT B.SALARY
                                   , B.BONUS
                                FROM EMP_SALARY B
                               WHERE B.EMP_NAME = '유재식'
                             )
 WHERE A.EMP_NAME = '방명수';

-- 방명수 사원의 급여 인상 소식을 전해들은 다른 직원들이
-- 단체로 파업을 진행했다.
-- 노옹철, 전형돈, 정중하, 하동운 사원의 급여와 보너스도
-- 유재식 사원의 급여 및 보너스와 같게 변경하는 UPDATE문을 작성하시오.
UPDATE
       EMP_SALARY A
   SET (A.SALARY, A.BONUS) = (SELECT B.SALARY
                                   , B.BONUS
                                FROM EMP_SALARY B
                               WHERE B.EMP_NAME = '유재식'
                             )
 WHERE A.EMP_NAME IN ('노옹철', '전형돈', '정중하', '하동운');
 
SELECT 
       A.*
  FROM EMP_SALARY A
 WHERE A.EMP_NAME IN ('유재식', '노옹철', '전형돈', '정중하', '하동운');

-- 다중행 서브쿼리를 활용한 UPDATE
-- EMP_SALARY 테이블에서 아시아 지역에 근무하는 직원의
-- 보너스를 0.5로 변경하시오.
SELECT
       A.EMP_ID
     , C.LOCAL_NAME
  FROM EMPLOYEE A
  JOIN DEPARTMENT B ON (A.DEPT_CODE = B.DEPT_ID)
  JOIN LOCATION C ON (B.LOCATION_ID = C.LOCAL_CODE)
 WHERE C.LOCAL_NAME LIKE 'ASIA%';
 
UPDATE
       EMP_SALARY A
   SET A.BONUS = 0.5
 WHERE A.EMP_ID IN (SELECT A.EMP_ID
                      FROM EMPLOYEE A
                      JOIN DEPARTMENT B ON (A.DEPT_CODE = B.DEPT_ID)
                      JOIN LOCATION C ON (B.LOCATION_ID = C.LOCAL_CODE)
                     WHERE C.LOCAL_NAME LIKE 'ASIA%'
                   );

 
-- UPDATE 시 변경할 값은 해당 컬럼에 대한 제약조건에 위배되지 않도록 해야 함
UPDATE
       EMPLOYEE A
   SET A.DEPT_CODE = '10';  -- FOREIGN KEY 제약조건 위배 됨

UPDATE
       EMPLOYEE A
   SET A.EMP_NAME = NULL;   -- NOT NULL 제약조건 위배 됨

UPDATE
       EMPLOYEE A
   SET A.EMP_NO = '621235-1985634'
 WHERE A.EMP_ID = '201';    -- UNIQUE 제약조건 위배 됨

SELECT * FROM EMPLOYEE;

-- DELETE: 테이블의 행을 삭제하는 구문이다.
--         테이블의 행의 갯수가 줄어든다.
-- DELETE FROM 테이블명 [WHERE 조건절]
-- 만약 WHERE 조건을 설정하지 않으면 모든 행이 다 삭제된다.
SELECT * FROM EMP_SALARY;
DELETE
  FROM EMP_SALARY
 WHERE EMP_ID = '901'; -- WHERE 절로 행을 선택할 때는 PK 컬럼의 값으로 조건을 작성하자.
  
ROLLBACK;

SELECT * FROM EMPLOYEE;

DELETE
  FROM DEPARTMENT A
 WHERE A.DEPT_ID = 'D1'; -- FOREIGN KEY 제약조건(삭제룰 적용 안된)이 설정되어 부모 테이블의 행을 삭제할 수 없다.
  
SELECT
       B.CONSTRAINT_NAME
     , B.COLUMN_NAME
  FROM USER_CONSTRAINTS A
  JOIN USER_CONS_COLUMNS B ON (A.CONSTRAINT_NAME = B.CONSTRAINT_NAME)
 WHERE B.TABLE_NAME = 'EMPLOYEE';   -- SYS_C007565

-- 제약 조건 잠시 끄기
ALTER TABLE EMPLOYEE
DISABLE CONSTRAINT SYS_C007565;

DELETE
  FROM DEPARTMENT A
 WHERE A.DEPT_ID = 'D1'; -- 제약조건이 무효화 되어 부모테이블 행 삭제 가능

-- 제약 조건 다시 켜기
UPDATE
       EMPLOYEE A
   SET A.DEPT_CODE = NULL
 WHERE A.DEPT_CODE = 'D1';

ALTER TABLE EMPLOYEE
ENABLE CONSTRAINT SYS_C007565;

-- TRUNCATE: 테이블의 전체 행을 삭제할 시 사용한다.
--           DELETE보다 수행 속도가 더 빠르다.
--           ROLLBACK을 통해 복구할 수 없다.
SELECT * FROM EMP_SALARY;
        
TRUNCATE TABLE EMP_SALARY; -- 테이블 초기화
ROLLBACK;

-- MERGE: 구조가 같은 두 개의 테이블을 하나의 테이블을 기준으로 합치기(병합) 기능을 한다.
--        테이블에서 지정하는 조건의 값이 존재하면 UPDATE, 조건의 값이 없으면 INSERT 된다.
CREATE TABLE EMP_M01
AS
SELECT A.*
  FROM EMPLOYEE A;
  
CREATE TABLE EMP_M02
AS
SELECT A.*
  FROM EMPLOYEE A
 WHERE A.JOB_CODE = 'J4';

INSERT
  INTO EMP_M02
VALUES
(
  999, '김용승', '000101-3123456', 'dlagon77@hanmail.net', '01092880100'
, 'D9', 'J4', 'S1', 9000000, 0.5
, NULL, SYSDATE, NULL, DEFAULT
);

UPDATE
       EMP_M02 A
   SET A.SALARY = 0;
ROLLBACK;

 MERGE
  INTO EMP_M01 A
 USING EMP_M02 B
    ON (A.EMP_ID = B.EMP_ID)
  WHEN MATCHED THEN
UPDATE
   SET A.EMP_NAME = B.EMP_NAME
     , A.EMP_NO = B.EMP_NO
     , A.EMAIL = B.EMAIL
     , A.PHONE = B.PHONE
     , A.DEPT_CODE = B.DEPT_CODE
     , A.JOB_CODE = B.JOB_CODE
     , A.SAL_LEVEL = B.SAL_LEVEL
     , A.SALARY = B.SALARY
     , A.BONUS = B.BONUS
     , A.MANAGER_ID = B.MANAGER_ID
     , A.HIRE_DATE = B.HIRE_DATE
     , A.ENT_DATE = B.ENT_DATE
     , A.ENT_YN = B.ENT_YN
  WHEN NOT MATCHED THEN
INSERT
(
  A.EMP_ID, A.EMP_NAME, A.EMP_NO, A.EMAIL, A.PHONE
, A.DEPT_CODE, A.JOB_CODE, A.SAL_LEVEL, A.SALARY, A.BONUS
, A.MANAGER_ID, A.HIRE_DATE, A.ENT_DATE, A.ENT_YN
)
VALUES
(
  B.EMP_ID, B.EMP_NAME, B.EMP_NO, B.EMAIL, B.PHONE
, B.DEPT_CODE, B.JOB_CODE, B.SAL_LEVEL, B.SALARY, B.BONUS
, B.MANAGER_ID, B.HIRE_DATE, B.ENT_DATE, B.ENT_YN
);

SELECT * FROM EMP_M01;
SELECT * FROM EMP_M02;
           
           
           
           
           
           
           
           
           
           
           






