-- SUBQUERY(서브쿼리)
-- 서브쿼리: 쿼리문 안에서 사용 된 쿼리문
 
-- 노옹철 사원과 같은 부서에서 일하는 직원 명단 조회
-- 1. 노옹철 사원의 부서(SUB QUERY)
SELECT
       DEPT_CODE
  FROM EMPLOYEE A
 WHERE A.EMP_NAME = '노옹철';  -- D9
  
-- 2. 같은 부서에서 일하는 직원 명단 조회(MAIN QUERY)
SELECT
       *
  FROM EMPLOYEE A
 WHERE A.DEPT_CODE = 'D9'
   AND A.EMP_NAME != '노옹철'; -- 조회 결과 2명
 
-- 서브쿼리 활용해 보기
SELECT
       *
  FROM EMPLOYEE A
 WHERE A.DEPT_CODE = (SELECT B.DEPT_CODE
                        FROM EMPLOYEE B
                       WHERE B.EMP_NAME = '노옹철'
                     )
   AND A.EMP_NAME != '노옹철';

-- 서브쿼리의 유형
-- 단일행 서브쿼리: 서브쿼리의 조회 결과 행의 갯수가 1개인 경우
-- 다중행 서브쿼리: 서브쿼리의 조회 결과 행의 갯수가 여러개인 경우
-- 다중열 서브쿼리: 서브쿼리의 조회 결과 행의 열이 여러개인 경우
-- 다중행 다중열 서브쿼리: 서브쿼리의 조회 결과 행의 수와 열의 수가 여러개인 경우
 
-- 서브쿼리의 유형에 따라 서브쿼리 앞에 붙는 연산자가 다르다.
-- 단일행 서브쿼리 앞에는 기존의 일반 비교 연산자 사용
-- >, <, >=, <=, =, !=/<>/^= (서브쿼리)
 
-- 노옹철 사원의 급여보다 많이 받는 사원의
-- 사번, 이름, 부서코드, 직급코드, 급여를 조회
SELECT
       A.SALARY
  FROM EMPLOYEE A
 WHERE A.EMP_NAME = '노옹철'; -- 3700000, 단일행
 
SELECT
       A.EMP_ID
     , A.EMP_NAME
     , A.DEPT_CODE
     , A.JOB_CODE
     , A.SALARY
  FROM EMPLOYEE A
 WHERE A.SALARY > (SELECT B.SALARY
                     FROM EMPLOYEE B
                    WHERE B.EMP_NAME = '노옹철'
                  );
                  
-- 가장 적은 급여를 받는 직원의
-- 사번, 이름, 직급코드, 부서코드, 급여, 입사일을 조회
SELECT MIN(A.SALARY)
  FROM EMPLOYEE A;
  
SELECT
       A.EMP_ID
     , A.EMP_NAME
     , A.JOB_CODE
     , A.DEPT_CODE
     , A.SALARY
     , A.HIRE_DATE
  FROM EMPLOYEE A
 WHERE A.SALARY = (SELECT MIN(B.SALARY)
                     FROM EMPLOYEE B
                  );
                  
-- 다중행 서브쿼리
-- 다중행 서브쿼리 앞에서는 일반 비교 연산자를 사용할 수 없다.
-- IN / NOT IN: 여러 개의 결과 값 중에서 한 개라도 일치하는 값이 있다면
--              혹은 없다면이라는 의미
-- > ANY / < ANY: 여러 개의 결과 값 중에서 한 개라도 큰 / 작은 경우
--                가장 작은 값보다 크냐? / 가장 큰 값보다 작냐?
--                (서브쿼리의 결과들 중에 어떤 것보다도 크거나 작기만 하면 된다.)
-- > ALL / < ALL: 모든 값보다 큰 / 작은 경우
--                가장 큰 값보다 크냐? / 가장 작은 값보다 작냐?
--                (모든 서브쿼리의 결과들보다 크거나 작아야 한다.)
-- EXISTS / NOT EXISTS: 서브쿼리에만 사용하는 연산자로
--                      조회 값이 존재하냐? / 존재하니 않느냐?
 
-- 부서별 최고 급여를 받는 직원의 이름, 직급 코드, 부서 코드, 급여 조회
SELECT MAX(A.SALARY)
     , A.DEPT_CODE
  FROM EMPLOYEE A
 WHERE A.DEPT_CODE IS NOT NULL
 GROUP BY A.DEPT_CODE;
  
SELECT
       A.EMP_NAME
     , A.JOB_CODE
     , A.DEPT_CODE
     , A.SALARY
  FROM EMPLOYEE A
 WHERE A.SALARY IN (SELECT MAX(B.SALARY)
                      FROM EMPLOYEE B
                     WHERE B.DEPT_CODE IS NOT NULL
                     GROUP BY B.DEPT_CODE
                   );
                   
-- 관리자에 해당하는 직원에 대한 정보와 관리자가 아닌 직원의
-- 정보를 추출하여 조회
-- (사번, 이름, 부서명, 직급명, '관리자' "구분" / '직원' "구분")
-- (상사를 두고 있는 사원들(MANAGER_ID가 NULL이 아닌 사람들)의 상사를 중복값 없이 추출)
SELECT * FROM EMPLOYEE;

-- 관리자에 해당하는 직원
SELECT DISTINCT A.MANAGER_ID
  FROM EMPLOYEE A
 WHERE A.MANAGER_ID IS NOT NULL;
 
SELECT
       A.EMP_ID
     , A.EMP_NAME
     , B.DEPT_TITLE
     , C.JOB_NAME
     , '관리자' "구분"
  FROM EMPLOYEE A
  LEFT JOIN DEPARTMENT B ON (A.DEPT_CODE = B.DEPT_ID)
  LEFT JOIN JOB C ON (A.JOB_CODE = C.JOB_CODE)
 WHERE A.EMP_ID IN (SELECT DISTINCT D.MANAGER_ID
                      FROM EMPLOYEE D
                     WHERE D.MANAGER_ID IS NOT NULL
                   );
 
-- 관리자에 해당하지 않는 직원
SELECT
       A.EMP_ID
     , A.EMP_NAME
     , B.DEPT_TITLE
     , C.JOB_NAME
     , '직원' "구분"
  FROM EMPLOYEE A
  LEFT JOIN DEPARTMENT B ON (A.DEPT_CODE = B.DEPT_ID)
  LEFT JOIN JOB C ON (A.JOB_CODE = C.JOB_CODE)
 WHERE A.EMP_ID NOT IN (SELECT DISTINCT A.MANAGER_ID
                          FROM EMPLOYEE A
                         WHERE A.MANAGER_ID IS NOT NULL
                       ); 
                       
-- 한번에 조회
SELECT
       A.EMP_ID
     , A.EMP_NAME
     , B.DEPT_TITLE
     , C.JOB_NAME
     , '관리자' "구분"
  FROM EMPLOYEE A
  LEFT JOIN DEPARTMENT B ON (A.DEPT_CODE = B.DEPT_ID)
  LEFT JOIN JOB C ON (A.JOB_CODE = C.JOB_CODE)
 WHERE A.EMP_ID IN (SELECT DISTINCT D.MANAGER_ID
                      FROM EMPLOYEE D
                     WHERE D.MANAGER_ID IS NOT NULL
                   )
UNION
SELECT
       A.EMP_ID
     , A.EMP_NAME
     , B.DEPT_TITLE
     , C.JOB_NAME
     , '직원' "구분"
  FROM EMPLOYEE A
  LEFT JOIN DEPARTMENT B ON (A.DEPT_CODE = B.DEPT_ID)
  LEFT JOIN JOB C ON (A.JOB_CODE = C.JOB_CODE)
 WHERE A.EMP_ID NOT IN (SELECT DISTINCT D.MANAGER_ID
                          FROM EMPLOYEE D
                         WHERE D.MANAGER_ID IS NOT NULL
                       )
 ORDER BY 5; 
 
-- 차장 직급 급여의 가장 큰 값보다 많이 받는 과장 직급의 (모든 차장들보다 급여를 많이 받는 과장)
-- 사번, 이름, 직급명, 급여를 조회
-- (단, > ALL 혹은 < ALL 연산자를 이용)

-- 차장 직급들의 급여
SELECT A.SALARY
  FROM EMPLOYEE A
  JOIN JOB B ON (A.JOB_CODE = B.JOB_CODE)
 WHERE B.JOB_NAME = '차장';
 
SELECT
       A.EMP_ID
     , A.EMP_NAME
     , B.JOB_NAME
     , A.SALARY
  FROM EMPLOYEE A
  JOIN JOB B ON (A.JOB_CODE = B.JOB_CODE)
 WHERE B.JOB_NAME = '과장'
   AND A.SALARY > ALL (SELECT C.SALARY
                         FROM EMPLOYEE C
                         JOIN JOB D ON (C.JOB_CODE = D.JOB_CODE)
                        WHERE D.JOB_NAME = '차장'
                      );
                      
-- 대리 직급의 직원들 중에서 과장 직급의 최소 급여보다 많이 받는(과장 중에 아무나 한명 보다만 많이 받으면 됨)
-- 직원의 사번, 이름, 직급명, 급여를 조회
-- (단, > ANY 혹은 < ANY 연산자 사용)
SELECT A.SALARY
  FROM EMPLOYEE A
  JOIN JOB B ON (A.JOB_CODE = B.JOB_CODE)
 WHERE B.JOB_NAME = '과장';
 
SELECT
       A.EMP_ID
     , A.EMP_NAME
     , B.JOB_NAME
     , A.SALARY
  FROM EMPLOYEE A
  JOIN JOB B ON (A.JOB_CODE = B.JOB_CODE)
 WHERE B.JOB_NAME = '대리'
   AND A.SALARY > ANY (SELECT C.SALARY
                         FROM EMPLOYEE C
                         JOIN JOB D ON (C.JOB_CODE = D.JOB_CODE)
                        WHERE D.JOB_NAME = '과장'
                      );
  
-- EXISTS / NOT EXISTS
-- 서브 쿼리 조회 결과가 있는지 없는지
SELECT A.EMP_NAME
  FROM EMPLOYEE A
 WHERE A.EMP_ID = '100';
  
SELECT
       A.EMP_NAME
  FROM EMPLOYEE A
 WHERE NOT EXISTS (SELECT B.EMP_NAME
                     FROM EMPLOYEE B
                    WHERE B.EMP_ID = '100'
                  );
                  
-- 조회를 할 지 여부에 대한 ON/OFF 같은 코드
SELECT
       *
  FROM EMPLOYEE A
-- WHERE 1 = 0; -- OFF
 WHERE 1 = 1;  -- ON
  
-- 자신의 직급의 평균 급여를 받고 있는 직원의
-- 사번, 이름, 직급코드, 급여를 조회
-- (단, 급여와 급여 평균은 만원 단위로 계산하시오.(TRUNC(컬럼명, -4)))
SELECT
       TRUNC(12345, -4)
  FROM DUAL;
  
-- 직급별 평균 급여(만원 단위)
SELECT
       TRUNC(AVG(A.SALARY), -4)
  FROM EMPLOYEE A
 GROUP BY A.JOB_CODE;
 
-- 직급별 평균과 일치하는 급여를 받는 사원 조회(다른 직급의 급여 평균과 일치해도 조회 됨)
SELECT
       A.EMP_ID
     , A.EMP_NAME
     , A.JOB_CODE
     , A.SALARY
  FROM EMPLOYEE A
 WHERE TRUNC(A.SALARY, -4) IN (SELECT TRUNC(AVG(B.SALARY), -4)
                                 FROM EMPLOYEE B
                                GROUP BY B.JOB_CODE
                              );
  
-- 직급별 평균과 일치하며 그 직급이 자신의 직급인 사원 조회(다중열 서브쿼리)
SELECT
       A.EMP_ID
     , A.EMP_NAME
     , A.JOB_CODE
     , A.SALARY
  FROM EMPLOYEE A
 WHERE (TRUNC(A.SALARY, -4), A.JOB_CODE) IN (SELECT TRUNC(AVG(B.SALARY), -4)
                                                  , B.JOB_CODE
                                               FROM EMPLOYEE B
                                              GROUP BY B.JOB_CODE
                                            ); 
  
-- 퇴사한 여직원과 같은 부서, 같은 직급에 해당하는
-- 사원의 이름, 직급, 부서, 입사일을 조회
 
-- 퇴사한 여직원의 부서와 직급 조회(다중행 다중열 서브쿼리)
SELECT B.DEPT_CODE
     , B.JOB_CODE
  FROM EMPLOYEE B
 WHERE B.ENT_YN = 'Y'
   AND SUBSTR(B.EMP_NO, 8, 1) = '2';

SELECT
       A.EMP_NAME
     , A.JOB_CODE
     , A.DEPT_CODE
     , A.HIRE_DATE
  FROM EMPLOYEE A
 WHERE (A.DEPT_CODE, A.JOB_CODE) IN (SELECT B.DEPT_CODE
                                          , B.JOB_CODE
                                       FROM EMPLOYEE B
                                      WHERE B.ENT_YN = 'Y'
                                        AND SUBSTR(B.EMP_NO, 8, 1) = '2'
                                    )
--   AND A.EMP_ID NOT IN (SELECT C.EMP_ID
--                          FROM EMPLOYEE C
--                         WHERE C.ENT_YN = 'Y'
--                           AND SUBSTR(C.EMP_NO, 8, 1) = '2'
--                       );
   AND A.ENT_YN = 'N';
   
-- 서브쿼리의 사용 위치
-- : SELECT 절, FROM 절, WHERE 절, GROUP BY 절, HAVING 절, ORDER BY 절
-- DML 구문: INSERT문, UPDATE문, DELETE문
-- DDL 구문: CREATE TABLE문, CREATE VIEW문

-- 인라인 뷰(INLINE VIEW): FROM절에 쓰는 서브쿼리(테이블 대신에 사용)
--                        서브쿼리가 만든 결과 집합(RESULT SET)으로부터 시작을 의미
 
-- 직급별 급여 평균(만원단위), 직급코드
SELECT
       TRUNC(AVG(A.SALARY), -4)
     , A.JOB_CODE  
  FROM EMPLOYEE A
 GROUP BY A.JOB_CODE;
 
-- 인라인 뷰에서 연산으로 처리된 컬럼은 메인 쿼리에서 별칭으로만 사용 가능하다.
SELECT
       B."평균 급여(만원단위)"
     , B."직급 코드"
  FROM (SELECT
               TRUNC(AVG(A.SALARY), -4) "평균 급여(만원단위)"
             , A.JOB_CODE "직급 코드"
          FROM EMPLOYEE A
         GROUP BY A.JOB_CODE
       ) B;
       
-- 위와 같은 인라인 뷰에서 사원명, 급여, 직급명을 추가로 알고 싶을 때
SELECT
       B."평균 급여(만원단위)"
     , B."직급 코드"
     , C.EMP_NAME
     , C.SALARY
     , D.JOB_NAME
  FROM (SELECT
               TRUNC(AVG(A.SALARY), -4) "평균 급여(만원단위)"
             , A.JOB_CODE "직급 코드"
          FROM EMPLOYEE A
         GROUP BY A.JOB_CODE
       ) B
  JOIN EMPLOYEE C ON (B."직급 코드" = C.JOB_CODE)   -- 인라인 뷰가 별칭을 사용하면 메인 쿼리는 반드시 별칭으로만 접근 가능
  JOIN JOB D ON (C.JOB_CODE = D.JOB_CODE);

-- ★★★ 인라인 뷰를 활용한 TOP-N 분석 ★★★
-- ROWNUM: 행 번호(순번)을 의미함
--         FROM의 결과 행(튜플)에 자동으로 순번이 달리게 된다.
--         ROWNUM을 활용한 조건절에서는 반드시 1순위부터 포함되게 범위를 지정해야 한다. ★★★
  
SELECT
       ROWNUM       -- ROWNUM에는 테이블 별칭을 달면 조회되지 않는다.
     , A.SALARY
  FROM EMPLOYEE A
 ORDER BY 2;
 
-- 급여를 많이 받는 사원을 10명 추출
-- 잘못된 방법
SELECT
       ROWNUM
     , A.SALARY
  FROM EMPLOYEE A
 WHERE ROWNUM <= 10
 ORDER BY 2 DESC;  
 
-- 잘된 방법
SELECT A.SALARY
  FROM EMPLOYEE A
 ORDER BY 1 DESC;
 
SELECT
       ROWNUM
     , B.SALARY
  FROM (SELECT A.SALARY
          FROM EMPLOYEE A
         ORDER BY 1 DESC
       ) B
 WHERE ROWNUM <= 10;
  
-- ROWNUM이 1을 반드시 포함해야 한다는 규칙을 지키지 않기 위한 2중 서브쿼리
-- ROWNUM을 1부터의 범위가 아닌 원하는 범위로 하려면 한번더 메인 쿼리 개념을 덧 씌우면 된다.(반드시 별칭 사용할 것)
SELECT
       C."순번"
     , C.SALARY
  FROM (SELECT
               ROWNUM "순번"
             , B.SALARY
          FROM (SELECT A.SALARY
                  FROM EMPLOYEE A
                 ORDER BY 1 DESC
               ) B
         WHERE ROWNUM <= 10
       ) C
 WHERE "순번" >= 2 AND "순번" <= 5;
       
-- 부서별 급여 평균 상위 3위 안에 드는 부서들의
-- 부서코드와 부서명, 평균 급여를 조회
-- 첫번째 방법(인라인 뷰에서 제공하는 컬럼 외에 나머지 컬럼은 메인쿼리에서 조인으로 해결)
SELECT AVG(A.SALARY)
     , A.DEPT_CODE
  FROM EMPLOYEE A
 WHERE A.DEPT_CODE IS NOT NULL
 GROUP BY A.DEPT_CODE
 ORDER BY 1 DESC;

SELECT
       B."부서별 평균급여"
     , B.DEPT_CODE
     , C.DEPT_TITLE
  FROM (SELECT AVG(A.SALARY) "부서별 평균급여"
             , A.DEPT_CODE
          FROM EMPLOYEE A
         WHERE A.DEPT_CODE IS NOT NULL
         GROUP BY A.DEPT_CODE
         ORDER BY 1 DESC
       ) B
  JOIN DEPARTMENT C ON (B.DEPT_CODE = C.DEPT_ID)
 WHERE ROWNUM <= 3;
 
-- 두번째 방법(인라인 뷰에서 문제에서 요구한 모든 컬럼을 미리 제공하는 방법으로 해결)
SELECT AVG(A.SALARY) "부서별 평균 급여"
     , A.DEPT_CODE
     , B.DEPT_TITLE
  FROM EMPLOYEE A
  JOIN DEPARTMENT B ON (A.DEPT_CODE = B.DEPT_ID)
 GROUP BY A.DEPT_CODE, B.DEPT_TITLE
 ORDER BY 1 DESC;
 
SELECT
       C."부서별 평균 급여"
     , C.DEPT_CODE
     , C.DEPT_TITLE
  FROM (SELECT AVG(A.SALARY) "부서별 평균 급여"
             , A.DEPT_CODE
             , B.DEPT_TITLE
          FROM EMPLOYEE A
          JOIN DEPARTMENT B ON (A.DEPT_CODE = B.DEPT_ID)
         GROUP BY A.DEPT_CODE, B.DEPT_TITLE
         ORDER BY 1 DESC
       ) C
 WHERE ROWNUM <= 3;

-- 서브쿼리(인라인 뷰)는 결과 및 정렬을 담당하고 메인 쿼리는 잘라내기를 담당한다.

-- WITH 이름 AS (서브쿼리문)
-- 서브쿼리에 이름을 붙여주고 메인쿼리에서 사용 시 이름을 사용함
-- 인라인 뷰로 사용 될 서브쿼리에서 사용하며
-- 같은 서브쿼리가 여러 번 사용 될 경우 중복 작성을 줄일 수 있다.
-- 실행 속도도 빨라지고 가독성도 좋아지는 장점을 가진다.
 
WITH
     TOPN_SAL
  AS (SELECT A.EMP_ID
           , A.EMP_NAME
           , A.SALARY
        FROM EMPLOYEE A
       ORDER BY A.SALARY DESC
     )
SELECT
       ROWNUM
     , B.EMP_NAME
     , B.SALARY
  FROM TOPN_SAL B
 WHERE ROWNUM <= 3;

-- RANK() 함수
-- : 동일한 순위 이후의 등수를 동일한 인원수만큼 건너 뛰고 다음 순위를 계산하는 방식
-- DENSE_RANK() 함수
-- : 중복되는 순위 이후의 등수를 건너뛰지 않고 이후 순위를 처리

--  RANK관련 함수는 정렬도 해줌(ORDER BY 따로 할 필요 없음)
SELECT
       A.EMP_NAME
     , A.SALARY
     , RANK() OVER(ORDER BY A.SALARY DESC) "순위"
  FROM EMPLOYEE A;
 
SELECT
       A.EMP_NAME
     , A.SALARY
     , DENSE_RANK() OVER(ORDER BY A.SALARY DESC) "순위"
  FROM EMPLOYEE A;

-- RANK() OVER나 DENSE_RANK() OVER를 활용해서도 TOP-N 분석을 할 수 있다.(2중 서브쿼리 안해도 TOP-N분석 할 수 있게)
SELECT
       B.EMP_NAME
     , B.SALARY
     , B."순위"
  FROM (SELECT A.EMP_NAME
             , A.SALARY
             , RANK() OVER(ORDER BY A.SALARY DESC) "순위"
          FROM EMPLOYEE A
       ) B
 WHERE "순위" >= 2
   AND "순위" <= 5;
   
-- 서브쿼리를 다양한 곳에서 써보자.

-- 부서별 급여 합계가 전체 급여의 총 합의 20%보다 많은
-- 부서의 부서명과 부서별 급여 합계 조회
SELECT SUM(C.SALARY) * 0.2
  FROM EMPLOYEE C;
 
SELECT
       SUM(A.SALARY)
     , B.DEPT_TITLE
  FROM EMPLOYEE A
  JOIN DEPARTMENT B ON (A.DEPT_CODE = B.DEPT_ID)
 GROUP BY B.DEPT_TITLE
HAVING SUM(A.SALARY) > (SELECT SUM(C.SALARY) * 0.2
                          FROM EMPLOYEE C
                       );
                       
-- 인라인 뷰 방식으로도 풀어보자.(HAVING말고 WHERE로 처리 해보기)
SELECT
       SUM(A.SALARY)
     , B.DEPT_TITLE
  FROM EMPLOYEE A
  JOIN DEPARTMENT B ON (A.DEPT_CODE = B.DEPT_ID)
 GROUP BY B.DEPT_TITLE;

SELECT
       C."SSAL"
     , C."DT"
  FROM (SELECT
               SUM(A.SALARY) "SSAL"
             , B.DEPT_TITLE "DT"
          FROM EMPLOYEE A
          JOIN DEPARTMENT B ON (A.DEPT_CODE = B.DEPT_ID)
         GROUP BY B.DEPT_TITLE
       ) C
 WHERE "SSAL" > (SELECT SUM(D.SALARY) * 0.2
                   FROM EMPLOYEE D
                );
 
-- 상[호연]관 서브쿼리
-- 일반적으로는 서브쿼리가 만든 결과 값을 메인 쿼리가 비교 연산
-- 메인 쿼리가 사용하는 테이블의 값을 서브쿼리가 이용해서 결과를 만듦
-- 메인 쿼리의 테이블 값이 변경되면, 서브쿼리의 결과값도 매번 바뀌게 된다.
-- (메인 쿼리의 영향을 받은 서브쿼리의 결과가 매번 달라진다는 의미)
 
-- 관리자 사번이 EMPLOYEE 테이블에 EMP_ID로 존재하는 직원에 대한 조회
SELECT
       A.EMP_ID
     , A.EMP_NAME
     , A.DEPT_CODE
     , A.MANAGER_ID
  FROM EMPLOYEE A
 WHERE EXISTS (SELECT B.EMP_ID
                 FROM EMPLOYEE B
                WHERE A.MANAGER_ID = B.EMP_ID
              );    -- 아직 사원으로 존재하는 관리자 사번을 가진 사원들만 조회

-- 스칼라 서브쿼리
-- 상관 서브쿼리 + 단일행 서브쿼리
SELECT
       A.EMP_ID
     , A.EMP_NAME
     , A.MANAGER_ID
     , NVL((SELECT B.EMP_NAME
              FROM EMPLOYEE B
             WHERE A.MANAGER_ID = B.EMP_ID
           ), '없음')
  FROM EMPLOYEE A; -- 아직 사원으로 존재하는 관리자의 이름을 같이 조회
  
  
  
  
  
  
  
  
  
  